<<<?xml version="1.0" encoding="UTF-8"?>
>>
TEI -xmlns:rng=http://relaxng.org/ns/structure/1.0 \
  -xmlns:sch=http://purl.oclc.org/dsdl/schematron \
  -xmlns=http://www.tei-c.org/ns/1.0
  teiHeader
    fileDesc
      titleStmt
        title
          <<Music Encoding Initiative Guidelines:>>
          title -type=sub <<Notre Dame Customization>>
        respStmt
          resp <<Authored by>>
          name -xml:id=JS <<Joshua Stutter>>
  text
    front
      divgen -type=toc
    body
      schemaSpec -ident=mei -start=<<mei meiHead meiCorpus music>> \
        -ns=http://www.music-encoding.org/ns/mei
        -- Include all modules
        -- Deactivate the CMN, cmnOrnaments modules
        moduleRef -key=MEI
        moduleRef -key=MEI.analytical
        -- moduleRef key=MEI.cmn
        -- moduleRef key=MEI.cmnOrnaments
        moduleRef -key=MEI.corpus
        moduleRef -key=MEI.critapp
        moduleRef -key=MEI.drama
        moduleRef -key=MEI.edittrans
        moduleRef -key=MEI.externalsymbols
        moduleRef -key=MEI.facsimile
        moduleRef -key=MEI.figtable
        moduleRef -key=MEI.fingering
        moduleRef -key=MEI.frbr
        moduleRef -key=MEI.genetic
        moduleRef -key=MEI.gestural
        moduleRef -key=MEI.harmony
        moduleRef -key=MEI.header
        moduleRef -key=MEI.lyrics
        moduleRef -key=MEI.mensural
        moduleRef -key=MEI.midi
        moduleRef -key=MEI.msDesc
        moduleRef -key=MEI.namesdates
        moduleRef -key=MEI.neumes
        moduleRef -key=MEI.performance
        moduleRef -key=MEI.ptrref
        moduleRef -key=MEI.shared
        moduleRef -key=MEI.stringtab
        moduleRef -key=MEI.text
        moduleRef -key=MEI.usersymbols
        moduleRef -key=MEI.visual
        -- Include SVG
        moduleRef \
          -url=https://www.tei-c.org/release/xml/tei/custom/schema/relaxng/svg11.rng \
          -prefix=svg_
          content
            rng:define -name=mei_model.graphicLike -combine=choice
              rng:ref -name=svg_svg
        -- Disable CMN-specific model classes in the shared module
        classSpec -ident=model.sectionPart.cmn -module=MEI.shared -type=model \
          -mode=delete
        classSpec -ident=model.layerPart.cmn -module=MEI.shared -type=model \
          -mode=delete
        -- Define the new module
        moduleSpec -ident=MEI.notredame
        classSpec -ident=att.divisio.vis -module=MEI.visual -type=atts
          desc <<Visual domain attributes>>
          classes
            memberOf -key=att.altSym
            memberOf -key=att.color
            memberOf -key=att.extSym
            memberOf -key=att.visibility
            memberOf -key=att.width
          attList
            attDef -ident=len -usage=opt
              desc <<States the length of the divisio in virtual units.>>
              datatype
                rng:data -type=decimal
                  rng:param -name=minExclusive <<0>>
            attDef -ident=place -usage=opt
              desc <<Denotes the staff location of the divisio>>
              datatype
                rng:ref -name=data.STAFFLOC
        elementSpec -ident=divisio -module=MEI.notredame -mode=add
          desc <<Vertical line to indicate context-sensitive division>>
          classes
            memberOf -key=att.common
            memberOf -key=att.facsimile
            memberOf -key=att.pointing
            memberOf -key=att.targetEval
            memberOf -key=att.divisio.vis
            memberOf -key=att.notredame.divisio
            memberOf -key=model.eventLike
        elementSpec -ident=plica -module=MEI.notredame -mode=add
          desc <<Final pointing line at end of ligature for a plicated note>>
          classes
            memberOf -key=att.common
            memberOf -key=att.facsimile
            memberOf -key=att.pointing
            memberOf -key=att.targetEval
            memberOf -key=att.stems
            memberOf -key=att.plica.vis
            memberOf -key=att.notredame.plica
            memberOf -key=model.eventLike
          content
            rng:zeroOrMore
              rng:choice
                memberOf -key=model.appLike
                memberOf -key=model.editLike
                memberOf -key=model.eventLike.mensural
                memberOf -key=model.eventLike.neumes
                memberOf -key=model.transcriptionLike
