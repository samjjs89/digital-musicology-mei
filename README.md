# Manually encoding Notre Dame Polyphony in MEI

## Prerequisites

- [pxsl-tools](https://github.com/tmoertel/pxsl-tools)
- [valac](https://wiki.gnome.org/Projects/Vala)
- [GXml](https://wiki.gnome.org/GXml)

Read the essay [here](essay.md)
